﻿namespace BrillianceCodingTest.Business
{
    public interface IArrayCalculation
    {
        int[] DeletePart(int position, int[] arr);
        int[] Reverse(int[] arr);
    }
}