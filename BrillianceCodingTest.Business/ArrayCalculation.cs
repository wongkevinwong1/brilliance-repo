﻿using System;

namespace BrillianceCodingTest.Business
{
    public class ArrayCalculation : IArrayCalculation
    {
        public int[] Reverse(int[] arr)
        {
            for (int i = 0; i < arr.Length/2; i++)
            {
                arr[i] = arr[arr.Length - i - 1] + arr[i];
                arr[arr.Length - i - 1] = arr[i] - arr[arr.Length - i - 1];
                arr[i] = arr[i] - arr[arr.Length - i - 1];
            }

            return arr;
        }

        public int[] DeletePart(int position, int[] arr)
        {
            var zeroBasedPosition = position - 1;
            int i = 0;
            for (i = 0; i < arr.Length; i++)
            {
                if(i== zeroBasedPosition)
                {
                    break;
                }
            }

            if(i < arr.Length)
            {
                for (int j = i+1; j < arr.Length; j++)
                {
                    arr[j - 1] = arr[j];
                }

                Array.Resize(ref arr, arr.Length - 1);
            }

            return arr;
        }
    }
}
