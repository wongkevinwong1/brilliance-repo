using BrillianceCodingTest.Business;
using NUnit.Framework;
using System;
using System.Linq;

namespace Tests
{
    public class ArrayCalculationTest
    {
        private IArrayCalculation _arrayCalculation;

        [SetUp]
        public void Setup()
        {
            _arrayCalculation = new ArrayCalculation();
        }

        [Test]
        public void Given4Array_WhenReversed_ThenReturnReversedResult()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3, 4 };
            var expectedResult = new[] { 4, 3, 2, 1 };

            //act
            var result = _arrayCalculation.Reverse(givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }

        [Test]
        public void Given3Array_WhenReversed_ThenReturnReversedResult()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3 };
            var expectedResult = new[] { 3, 2, 1 };

            //act
            var result = _arrayCalculation.Reverse(givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }

        [Test]
        public void Given1PositionAnd4Array_WhenDeletePart_ThenReturn3Array()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3,4 };
            var position = 1;
            var expectedResult = new[] { 2,3,4 };

            //act
            var result = _arrayCalculation.DeletePart(position,givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }
        
        [Test]
        public void Given3PositionAnd4Array_WhenDeletePart_ThenReturn3Array()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3, 4 };
            var position = 3;
            var expectedResult = new[] { 1, 2, 4 };

            //act
            var result = _arrayCalculation.DeletePart(position, givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }


        [Test]
        public void Given4PositionAnd4Array_WhenDeletePart_ThenReturn3Array()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3, 4 };
            var position = 4;
            var expectedResult = new[] { 1, 2, 3 };

            //act
            var result = _arrayCalculation.DeletePart(position, givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }


        [Test]
        public void Given5PositionAnd4Array_WhenDeletePart_ThenReturn4Array()
        {
            //arrange
            var givenInput = new[] { 1, 2, 3, 4 };
            var position = 5;
            var expectedResult = new[] { 1, 2, 3,4 };

            //act
            var result = _arrayCalculation.DeletePart(position, givenInput);

            //assert
            Assert.IsTrue(result.SequenceEqual(expectedResult));
        }
    }
}