using BrillianceCodingTest.API;
using BrillianceCodingTest.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;

namespace Tests
{
    public class ArrayCalcControllerTest
    {
        private Mock<IArrayCalculation> _mockIArrayCalculation;
        private Mock<ILogger<ArrayCalcController>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockIArrayCalculation = new Mock<IArrayCalculation>();
            _mockLogger = new Mock<ILogger<ArrayCalcController>>();
        }

        [Test]
        public void GivenIntegerArray_WhenCallingReverseEndpoint_ThenReturnHttpOkResult()
        {
            //arrange
            var arrayCalcController = new ArrayCalcController(_mockIArrayCalculation.Object, _mockLogger.Object);

            _mockIArrayCalculation.Setup(x => x.Reverse(It.IsAny<int[]>())).Returns(new[] { 3, 2, 1 });

            //act

            var result = arrayCalcController.Reverse(new[] { 1, 2, 3 });

            Assert.IsNotNull(result);
            Assert.IsTrue(result is OkObjectResult);
        }

        [Test]
        public void GivenIntegerArray_WhenErrorHappensOnReverse_ThenReturnBadRequestResult()
        {
            //arrange
            var arrayCalcController = new ArrayCalcController(_mockIArrayCalculation.Object, _mockLogger.Object);

            _mockIArrayCalculation.Setup(x => x.Reverse(It.IsAny<int[]>())).Throws<Exception>();

            //act

            var result = arrayCalcController.Reverse(new[] { 1, 2, 3 });

            Assert.IsNotNull(result);
            Assert.IsTrue(result is BadRequestObjectResult);
        }

        [Test]
        public void GivenIntegerArray_WhenCallingDeletePartEndpoint_ThenReturnHttpOkResult()
        {
            //arrange
            var arrayCalcController = new ArrayCalcController(_mockIArrayCalculation.Object, _mockLogger.Object);

            _mockIArrayCalculation.Setup(x => x.DeletePart(It.IsAny<int>(), It.IsAny<int[]>())).Returns(new[] { 3, 2, 1 });

            //act

            var result = arrayCalcController.DeletePart(2, new [] { 1, 2, 3 });

            Assert.IsNotNull(result);
            Assert.IsTrue(result is OkObjectResult);
        }

        [Test]
        public void GivenIntegerArray_WhenErrorHappensOnDeletePart_ThenReturnBadRequestResult()
        {
            //arrange
            var arrayCalcController = new ArrayCalcController(_mockIArrayCalculation.Object, _mockLogger.Object);

            _mockIArrayCalculation.Setup(x => x.DeletePart(It.IsAny<int>(),It.IsAny<int[]>())).Throws<Exception>();

            //act

            var result = arrayCalcController.DeletePart(2, new [] { 1, 2, 3 });

            Assert.IsNotNull(result);
            Assert.IsTrue(result is BadRequestObjectResult);
        }
    }
}