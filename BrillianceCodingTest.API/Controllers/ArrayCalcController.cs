﻿using BrillianceCodingTest.Business;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BrillianceCodingTest.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArrayCalcController : ControllerBase
    {
        private readonly IArrayCalculation _arrayCalculation;
        private readonly ILogger<ArrayCalcController> _logger;

        public ArrayCalcController(IArrayCalculation arrayCalculation, ILogger<ArrayCalcController> logger)
        {
            _arrayCalculation = arrayCalculation;
            _logger = logger;
        }

        [HttpGet("reverse")]
        public IActionResult Reverse( int[] productIds  )
        {
            try
            {   
                if (ModelState.IsValid)
                {
                    var result = _arrayCalculation.Reverse(productIds);
                    return Ok(result);
                }
                else
                {
                    throw new Exception("Invalid input");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error on Reverse api call");
                return BadRequest("Error happened when reversing input");
            }
        }

        
        [HttpGet("deletepart")]
        public IActionResult DeletePart(int position,  int[] productIds)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _arrayCalculation.DeletePart(position, productIds);
                    return Ok(result);
                }
                else
                {
                    throw new Exception("Invalid input");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error on DeletePart api call");
                return BadRequest("Error happened when removing member array");
            }
        }
    }
}
